import { Repo } from './repo';

export const REPOS: Repo[] = [
    {name: 'Repo 1', description: 'Repo number 1'},
    {name: 'Repo 2', description: 'Repo number 2'},
];
