export type Repo = {
    name: string;
    description: string;
    tag_list: string[];
    web_url: string;
};
