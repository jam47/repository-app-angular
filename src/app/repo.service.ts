import { Injectable } from '@angular/core';
import { Repo } from './repo';
import { REPOS } from './mock-repos';
import { Observable, of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RepoService {

  private reposUrl = 'https://gitlab.com/api/v4/users/2983467/starred_projects?per_page=100';  // URL to web api

  getRepos(): Observable<Repo[]> {
    return this.http.get<Repo[]>(this.reposUrl);
  }

  constructor(private http: HttpClient) { }
}
